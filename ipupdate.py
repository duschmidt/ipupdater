import urllib2 as u
from emailer import sendEmail
import logging as lg
path="/home/pi/python/ip/"
lg.basicConfig(filename=path+"ipupdate.log",level=lg.INFO,  format="%(asctime)s %(levelname)s %(message)s")
url='http://automation.whatismyip.com/n09230945.asp'
lg.info("Reading ip address")
ip=None
try:
	ip= u.urlopen(url).read()
	lg.info("ip is %s"%ip)
except:
	lg.error("Error reading ip address")
	lg.shutdown()
	exit()

oldIp=None
try:
	f=open(path+'oldip.dat')
	oldIp=f.read()
	f.close()
	lg.info('old ip was %s'%oldIp)
except:
	lg.info('no prior ip found')

if ip !=oldIp:
	lg.info('ip is updated to %s'%ip)
	try:
		sendEmail("new ip: %s"%ip,subject="RPi IP Update")
	except:
		lg.error("Error sending update email")
	f=open(path+'oldip.dat','w')
	f.write(ip)
	f.close()
else:
	lg.info("ip is unchanged")

lg.shutdown()
