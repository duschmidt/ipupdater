import smtplib as s
from email.mime.text import MIMEText

def sendEmail(msg,receiver='xianthrops@gmail.com', subject='Subject',sender='xianthrops@gmail.com'):
	config = eval(open('config.dat','r').read())
	con = s.SMTP(config['smtpServer'],config['smtpPort'])
	con.ehlo()
	con.starttls()
	con.ehlo()
	con.login(config['smtpUsername'],config['smtpPw'])
	msg=MIMEText(msg)
	msg['Subject']=subject
	msg['From']=sender
	msg['To']=receiver
	con.sendmail(sender,[receiver],msg.as_string())
	con.close()
if __name__=="__main__":
	print "is main"
	sendEmail('flab')
